import pylab
from numpy import array, linalg, random, sqrt, inf
import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
from matplotlib.patches import Circle, PathPatch
from mpl_toolkits.mplot3d import Axes3D 
import mpl_toolkits.mplot3d.art3d as art3d
from decimal import Decimal  


import math
import itertools
from gtk.keysyms import marker


def drawUnitCircle(p):

 for i in range(3000):
  data = array([random.rand()*2-1,random.rand()*2-1])
  #make sure that the data will not go beyond the 1.0 in the unit circle
  if linalg.norm(data,p) >0.99 and linalg.norm(data,p) <1.1:
   pylab.plot(data[0],data[1],'go')
 pylab.axis([-2.0, 2.0, -2.0, 2.0])
 # set label for the draw
 fig = pylab.gcf()
 fig.canvas.set_window_title('unit circle with p='+str(p))
 pylab.show()
 
 
def computeNorm(vectorValue):
     
    value=0.0
    for i in xrange(0,len(vectorValue)):
         for j in xrange(0,len(vectorValue)):
             if(vectorValue[j]==0):
                 print 'zero'
             tmpValue=vectorValue[i]/vectorValue[j];
             value+=math.log(tmpValue)*math.log(tmpValue)         
    return float(float(value)/6.0)
     
if __name__ == "__main__":
    # 1.4
    #drawUnitCircle(0.5)
    #1.5
    # we need to draw a circle that has all the points that are has distance equals 3 from the origin
    n = 100
    X1 = np.linspace(0.01,1,n)
    X2=  np.linspace(0.01,1,n)
    X3=  np.linspace(0.01,1,n)
    firstCordinates=[]
    secondCordinates=[]
    thirdCoordinates=[]
    zero_element=[1/float(3),1/float(3),1/float(3)]
    
    val=computeNorm(zero_element)
    print val 
    #print zero_element
    for i in xrange(0,len(X1)):
        for j in xrange(0,len(X2)):
            # because we have condition that all elements should be bigger than zero, so the last element can't be zero
            if((float)(X1[i]+X2[j])>=1.0):
                continue;
            
            new_vector=[float(X1[i]),float(X2[j]),float(1.0-(float)(X1[i]+X2[j]))]
            norm=computeNorm(new_vector)
            norm=math.sqrt(norm)
            if (norm >2.9 and norm <3.2):
                #print new_vector
                #print norm
                firstCordinates.append(new_vector[0])
                secondCordinates.append(new_vector[1])
                thirdCoordinates.append(new_vector[2])
                    
                    
    #points=np.vstack((firstCordinates,secondCordinates,thirdCoordinates))
    #plotData2D(points, 'unitCircle.pdf')
    print len(firstCordinates)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    
    #Axes3D.mouse_init()
    ax.scatter(firstCordinates,secondCordinates,thirdCoordinates,c="r",marker='o')
    plt.show()
    print "finish"
    
                    